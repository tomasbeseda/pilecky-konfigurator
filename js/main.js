$(document).click(function() {
    $('.menu__select-box-list').fadeOut(100);
    $('.tabs__gate').fadeOut(100);
    $('.options__list').fadeOut(100);
});

/* 
 * Otevreni listu select-boxu 
 */

$('.menu__select-box').click(function(event) {
    $('.menu__select-box-list').fadeIn(100);
    event.stopPropagation();
});
$('.options__item').click(function(event) {
    var getCurrent = $(this).attr('data-id')
    $('#js-list-' + getCurrent).fadeToggle(100);
    event.stopPropagation();
    $('.options__list').not('#js-list-' + getCurrent).fadeOut(100);
});

/* 
 * Select script 
 */

$('.js-select-box').on('click', function() {
    var getCurrent = $(this).attr('id'),
        data = $('#' + getCurrent).clone().removeAttr('id'),
        inputId = $(this).attr('input-id'),
        inputValue = $(this).attr('input-value'),
        holder = $(this).attr('data-holder');
    $('#' + holder).html(data);
    $('.menu__select-box-list').fadeOut(100);
    $('#' + inputId).val(inputValue).change();
});

/* 
 * Select gate script 
 */

$(document).on('click', '.js-gate-box', function() {
    var inputId = $(this).attr('input-id'),
        inputValue = $(this).attr('input-value'),
        side = $(this).attr('recap');
    $('.tabs__gate').fadeOut(100);
    $('#' + inputId).val(inputValue).change();
    $('.recap-' + side).text(inputValue);
    $('.recap-s-' + side).text(inputValue);
    $('#check-' + side).prop('checked', true).attr('data-width-id', inputValue + inputId);
    $('#' + inputValue + inputId).fadeIn(100);
});

/*
 * Unselect gate script
 */

$('.js-unselect').on('click', function() {
    var thisId = $(this).attr('id').replace('check-', ''),
        thisGateWidth = $(this).attr('data-width-id');
    $('#' + thisId).val('').change();
    $('.recap-' + thisId).text('-');
    $('.recap-s-' + thisId).text('– nevybrána –');
    $('#' + thisGateWidth).fadeOut(100);
});

/*
 * Open lightbox
 */
$('.js-lightbox').on('click', function() {
    var headline = $(this).attr('data-headline'),
        description = $(this).attr('data-description'),
        idForButton = $(this).parent().find('img').attr('input-id'),
        valueForButton = $(this).parent().find('img').attr('input-value');
    $('.lightbox').fadeIn(100);
    $('.lightbox__content--holder').html('<span class="lightbox__headline">' + headline + '</span><span class="lightbox__description">' + description + '</span><button class="lightbox__button js-gate-box js-lightbox-close" input-id="' + idForButton  + '" input-value="' + valueForButton + '" recap="' + idForButton + '">Vložit do konfigurátoru</button>').change();
});

$(document).on('click', '.js-lightbox-close', function() {
    $('.lightbox').fadeOut(100);
});

/* 
 * Tabs 
 */

$('ul.tabs li').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#" + tab_id).addClass('current');
});

/* 
 * Plus/minus input 
 */

$('.qtyplus').click(function(e) {
    e.preventDefault();
    side = $(this).attr('side');
    fieldName = $(this).attr('field');
    var currentVal = parseInt($('input[name=' + fieldName + ']').val());
    if (!isNaN(currentVal)) {
        $('input[name=' + fieldName + ']').val(currentVal + 1 + ' m').change();
        $('#' + side).addClass('tab__link--active');
        $('.preview__base-' + side).show();
        $('#recap-lenght-' + side).text(currentVal + 1 + ' m');
    } else {
        $('input[name=' + fieldName + ']').val(0 + ' m');
        $('#' + side).removeClass('tab__link--active');
        $('.preview__base-' + side).hide();
        $('#recap-lenght-' + side).text('-');
    }
});
$(".qtyminus").click(function(e) {
    e.preventDefault();
    side = $(this).attr('side');
    fieldName = $(this).attr('field');
    var currentVal = parseInt($('input[name=' + fieldName + ']').val());
    if (!isNaN(currentVal) && currentVal > 0) {
        $('input[name=' + fieldName + ']').val(currentVal - 1 + ' m').change();
        $('#' + side).addClass('tab__link--active');
        $('.preview__base-' + side).show();
        $('#recap-lenght-' + side).text(currentVal - 1 + ' m');
    } else {
        $('input[name=' + fieldName + ']').val(0 + ' m');
        $('#' + side).removeClass('tab__link--active');
        $('.preview__base-' + side).hide();
        $('#recap-lenght-' + side).text('-');
    }
});

/*
 * Vyber branky 
 */

$('.js-gate').click(function(event) {
    var gateName = $(this).attr('id');
    $('#open-' + gateName).toggle('slide', {
        direction: 'left'
    });
    $('.tabs__gate').not('#open-' + gateName).fadeOut(100);
    event.stopPropagation();
});
/* 
 * Odeslani a aktualizace dat ve formulari 
 */
var form = $('#form'),
    type = form.find("input[name='type']").val(),
    montage = form.find("input[name='montage']").val(),
    optionsHeight = form.find("input[name='options-height']").val(),
    optionsWidth = form.find("input[name='options-width']").val(),
    optionsRole = form.find("input[name='options-role']").val(),
    quantity1 = form.find("input[name='quantity1']").val(),
    quantity2 = form.find("input[name='quantity2']").val(),
    quantity3 = form.find("input[name='quantity3']").val(),
    quantity4 = form.find("input[name='quantity4']").val(),
    desk1 = form.find("input[name='desk1']").val(),
    desk2 = form.find("input[name='desk2']").val(),
    desk3 = form.find("input[name='desk3']").val(),
    desk4 = form.find("input[name='desk4']").val(),
    optionsGateHeightA = form.find("input[name='options-gate-height-a']").val(),
    optionsGateHeightB = form.find("input[name='options-gate-height-b']").val(),
    optionsGateHeightC = form.find("input[name='options-gate-height-c']").val(),
    optionsGateHeightD = form.find("input[name='options-gate-height-d']").val(),
    bigGateA = form.find("input[name='big-gate-a']").val(),
    bigGateB = form.find("input[name='big-gate-b']").val(),
    bigGateC = form.find("input[name='big-gate-c']").val(),
    bigGateD = form.find("input[name='big-gate-d']").val(),
    smallGateA = form.find("input[name='small-gate-a']").val(),
    smallGateB = form.find("input[name='small-gate-b']").val(),
    smallGateC = form.find("input[name='small-gate-c']").val(),
    smallGateD = form.find("input[name='small-gate-d']").val();


form.on('change', function() {
    $.ajax({
        url: "test.php",
        method: 'post',
        data: {
            type: type,
            montage: montage,
            optionsHeight: optionsHeight,
            optionsWidth: optionsWidth,
            optionsRole: optionsRole,
            quantity1: quantity1,
            quantity2: quantity2,
            quantity3: quantity3,
            quantity4: quantity4,
            desk1: desk1,
            desk2: desk2,
            desk3: desk3,
            desk4: desk4,
            optionsGateHeightA: optionsGateHeightA,
            optionsGateHeightB: optionsGateHeightB,
            optionsGateHeightC: optionsGateHeightC,
            optionsGateHeightD: optionsGateHeightD,
            bigGateA: bigGateA,
            bigGateB: bigGateB,
            bigGateC: bigGateC,
            bigGateD: bigGateD,
            smallGateA: smallGateA,
            smallGateB: smallGateB,
            smallGateC: smallGateC,
            smallGateD: smallGateD
        }
    });
});
